/*
    Game 2048.cpp
    Alojz Soltys 2020
*/

#include <iostream>
#include <time.h>
#include <conio.h>

using namespace std;

void dispMove(unsigned int (&vals)[4][4], unsigned int);
void generateNewNumber(unsigned int (*vals)[4][4]);
void numberGenerator(unsigned int(& values)[4][4], int&, int (&temp)[16]);

unsigned int moveLeft(unsigned int (&vals)[4][4], bool & movedL);
unsigned int moveUp(unsigned int (&vals)[4][4], bool & movedU);
unsigned int moveDown(unsigned int (&vals)[4][4], bool & movedD);
unsigned int moveRight(unsigned int (&vals)[4][4], bool & movedR);

bool gameStatus (unsigned int (&vals)[4][4]);
bool winningCondition(unsigned int vals[4][4], bool &vFlag);

void lastMoveRun(unsigned int (&backupVals)[4][4], unsigned int (&tempVals)[4][4], unsigned int inputVals [4][4], unsigned int &backupScore, unsigned int &temporary, unsigned int score);
void lastMoveBegin(unsigned int (&backupVals)[4][4], unsigned int (&tempVals)[4][4], unsigned int inputVals [4][4], unsigned int &backupScore, unsigned int &temporary, unsigned int score);
void restoreLastMove(unsigned int (&inputVals)[4][4], unsigned int (&tempVals)[4][4], unsigned int backupVals [4][4], unsigned int &score, unsigned int &temporary, unsigned int backupScore);

int main()
{
    enum arrow_keys {KEY_UP = 72, KEY_DOWN = 80, KEY_LEFT = 75, KEY_RIGHT = 77};
    cout << "\n\n\tPress any key to start new game\n\n";
    getch();
    beginOfProg:
    // creating new game
    unsigned int valArray[4][4] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};    // game space
    unsigned int previousVals[4][4];                                    // last move game space (step back move)
    unsigned int temporaryArray [4][4];                                 // intermediate game space (necessary for step back move)
    unsigned int totalScore = 0;
    unsigned int previousScore;
    unsigned int temporaryScore;
    bool movePossible = true;
    bool victoryFlag = false;
    generateNewNumber((&valArray));
    system("cls");
    dispMove(valArray, totalScore);
    lastMoveBegin(previousVals, temporaryArray, valArray, previousScore, temporaryScore, totalScore);

    while (1){ // run mode
        int c = getch(); //arrow keys send two numbers from which first is the same

        switch(c){
        case KEY_UP:    totalScore += moveUp(valArray, movePossible);
                        break;
        case KEY_DOWN:  totalScore += moveDown(valArray, movePossible);
                        break;
        case KEY_RIGHT: totalScore += moveRight(valArray, movePossible);
                        break;
        case KEY_LEFT:  totalScore += moveLeft(valArray, movePossible);
                        break;
        default:        if ((char)c == 'e'){        // end game
                            cout << "\n\n\t---- Program terminated ----\n\n";
                            return 0;
                        }
                        else if ((char)c == 'n')    // game restart
                            goto beginOfProg;
                        else if ((char)c == 'b'){   // step back
                            restoreLastMove(valArray, temporaryArray, previousVals, totalScore, temporaryScore, previousScore);
                            system("cls");
                            dispMove(valArray, totalScore);
                        }
                        break;
        }

        if (movePossible && (c == KEY_UP || c == KEY_DOWN || c == KEY_RIGHT || c == KEY_LEFT)){
            generateNewNumber((&valArray));
            lastMoveRun(previousVals, temporaryArray, valArray, previousScore, temporaryScore, totalScore);
            system("cls");
            dispMove(valArray, totalScore);

            if (gameStatus(valArray))
                cout << "\n\n\t-----   You have lost   -----\n\n";

            if (winningCondition(valArray, victoryFlag))
                cout << "\n\n\t----- Victory is yours! -----\n\n";
        }
        else if ((!movePossible) && (c == KEY_UP || c == KEY_DOWN || c == KEY_RIGHT || c == KEY_LEFT))
            cout << "Cannot move this way\n";
    }
    return 0;
}

void dispMove(unsigned int (&vals)[4][4], unsigned int score)
{
    cout << "Press 'b' to step back, 'e' to end program or 'n' for a new game\n";
    cout << "\n\tYour score: " << score << "\n\n";
    for(int y = 0;y < 4; y++){
        cout << "\t";
        for (int x = 0; x < 4; x++){
            if (vals[x][y] == 0)
                cout << ".\t";
            else
                cout << vals[x][y] << "\t";
        }
    cout << "\n\n";
    }
}

void generateNewNumber(unsigned int (*vals)[4][4])
{
    int temp[16] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    int counter = 0;
    srand(time(NULL));
    //creating temp array of empty places
    for (int i = 0; i < 16; i++){
        if (*(vals[0][0]+i) == 0){
            temp[counter] = i;
            counter++;
        }
    }

    if (counter == 16){ // New game
        numberGenerator((*vals), counter, temp);
        numberGenerator((*vals), counter, temp);
    }
    else
        numberGenerator((*vals), counter, temp);
}

void numberGenerator(unsigned int(& values)[4][4], int& counter, int (&temp)[16])
{
    int randomFreePosition = rand() % (counter);
    int probability = rand() % 10;
    if(probability == 9)
        *((&values)[0][0]+temp[randomFreePosition]) = 4;
    else
        *((&values)[0][0]+temp[randomFreePosition]) = 2;

    //removing positioned place from temp and decreasing number of possibilities
    for (int i = randomFreePosition; i < 15; i++)
            temp[i] = temp[i + 1];
    counter--;
}

unsigned int moveLeft(unsigned int (&vals)[4][4], bool & movedL)
{
    int x;
    int y;
    unsigned int tempy;
    int emptyPlace;
    unsigned int pointAddition = 0;
    movedL = false;

    for(y = 0; y < 4; y++){
        emptyPlace = 0;
        tempy = 0;
        for(x = 0; x < 4; x++){
            if (vals[x][y] == 0){
                emptyPlace++;
            }
            else if (vals[x][y] != 0 && tempy == 0){
                tempy = vals[x][y];
                if (emptyPlace != 0){
                    vals[x-emptyPlace][y] = vals[x][y];
                    vals[x][y] = 0;
                    movedL = true;
                }
            }
            else if (vals[x][y] != 0 && tempy != 0){
                if(vals[x][y] == tempy){
                    emptyPlace++;
                    vals[x-emptyPlace][y] *= 2;
                    pointAddition += vals[x-emptyPlace][y];
                    vals[x][y] = 0;
                    tempy = 0;
                    movedL = true;
                }
                else {
                    tempy = vals[x][y];
                    if(emptyPlace != 0){
                        vals[x-emptyPlace][y] = vals[x][y];
                        vals[x][y] = 0;
                        movedL = true;
                    }
                }
            }
        }
    }
    return pointAddition;
}

unsigned int moveUp(unsigned int (&vals)[4][4], bool & movedU)
{
    int x;
    int y;
    unsigned int tempy = 0;
    int emptyPlace = 0;
    unsigned int pointAddition = 0;
    movedU = false;

    for(x = 0; x < 4; x++){
        emptyPlace = 0;
        tempy = 0;
        for(y = 0; y < 4; y++){
            if (vals[x][y] == 0){
                emptyPlace++;
            }
            else if (vals[x][y] != 0 && tempy == 0){
                tempy = vals[x][y];
                if (emptyPlace != 0){
                    vals[x][y-emptyPlace] = vals[x][y];
                    vals[x][y] = 0;
                    movedU = true;
                }
            }
            else if (vals[x][y] != 0 && tempy != 0){
                if(vals[x][y] == tempy){
                    emptyPlace++;
                    vals[x][y-emptyPlace] *= 2;
                    pointAddition += vals[x][y-emptyPlace];
                    vals[x][y] = 0;
                    tempy = 0;
                    movedU = true;
                }
                else {
                    tempy = vals[x][y];
                    if(emptyPlace != 0){
                        vals[x][y-emptyPlace] = vals[x][y];
                        vals[x][y] = 0;
                        movedU = true;
                    }
                }
            }
        }
    }
    return pointAddition;
}

unsigned int moveDown(unsigned int (&vals)[4][4], bool & movedD)
{
    int x;
    int y;
    unsigned int tempy = 0;
    int emptyPlace = 0;
    unsigned int pointAddition = 0;
    movedD = false;

    for(x = 0; x < 4; x++){
        emptyPlace = 0;
        tempy = 0;
        for(y = 3; y >= 0; y--){
            if (vals[x][y] == 0){
                emptyPlace++;
            }
            else if (vals[x][y] != 0 && tempy == 0){
                tempy = vals[x][y];
                if (emptyPlace != 0){
                    vals[x][y+emptyPlace] = vals[x][y];
                    vals[x][y] = 0;
                    movedD = true;
                }
            }
            else if (vals[x][y] != 0 && tempy != 0){
                if(vals[x][y] == tempy){
                    emptyPlace++;
                    vals[x][y+emptyPlace] *= 2;
                    pointAddition += vals[x][y+emptyPlace];
                    vals[x][y] = 0;
                    tempy = 0;
                    movedD = true;
                }
                else {
                    tempy = vals[x][y];
                    if(emptyPlace != 0){
                        vals[x][y+emptyPlace] = vals[x][y];
                        vals[x][y] = 0;
                        movedD = true;
                    }
                }
            }
        }
    }
    return pointAddition;
}

unsigned int moveRight(unsigned int (&vals)[4][4], bool & movedR)
{
    int x;
    int y;
    unsigned int tempy = 0;
    int emptyPlace = 0;
    unsigned int pointAddition = 0;
    movedR = false;

    for(y = 0; y < 4; y++){
        emptyPlace = 0;
        tempy = 0;
        for(x = 3; x >= 0; x--){
            if (vals[x][y] == 0){
                emptyPlace++;
            }
            else if (vals[x][y] != 0 && tempy == 0){
                tempy = vals[x][y];
                if (emptyPlace != 0){
                    vals[x+emptyPlace][y] = vals[x][y];
                    vals[x][y] = 0;
                    movedR = true;
                }
            }
            else if (vals[x][y] != 0 && tempy != 0){
                if(vals[x][y] == tempy){
                    emptyPlace++;
                    vals[x+emptyPlace][y] *= 2;
                    pointAddition += vals[x+emptyPlace][y];
                    vals[x][y] = 0;
                    tempy = 0;
                    movedR = true;
                }
                else {
                    tempy = vals[x][y];
                    if(emptyPlace != 0){
                        vals[x+emptyPlace][y] = vals[x][y];
                        vals[x][y] = 0;
                        movedR = true;
                    }
                }
            }
        }
    }
    return pointAddition;
}

bool gameStatus (unsigned int (&vals)[4][4])
{
    int x;
    int y;

    // searching for empty places
    for (x = 0; x < 4; x++)
        for (y = 0; y < 4; y++)
            if(vals[x][y] == 0)
                return false;

    // searching for horizontal or vertical pairs
    for (x = 0; x < 4; x++)
        for (y = 0; y < 3; y++)
            if (vals[x][y] == vals[x][y+1])
                return false;

    for (y = 0; y < 4; y++)
        for (x = 0; x < 3; x++)
            if (vals[x][y] == vals[x+1][y])
                return false;
    // game is lost
    return true;
}

bool winningCondition(unsigned int vals[4][4], bool &vFlag)
{
    const unsigned int winNumber = 2048;

    if(vFlag == false){
        for (int i = 0; i < 4; i++){
            for (int j = 0; j < 4; j++){
                if(winNumber == vals[i][j]){
                    vFlag = true;
                    return true;
                }
            }
        }
    }
    return false;
}

void lastMoveRun(unsigned int (&backupVals)[4][4], unsigned int (&tempVals)[4][4], unsigned int inputVals [4][4], unsigned int &backupScore, unsigned int &temporary, unsigned int score)
{
    for (int x = 0; x < 4; x++){
        for (int y = 0; y < 4; y++){
            backupVals[x][y] = tempVals[x][y];
            tempVals[x][y] = inputVals[x][y];
        }
    }

    backupScore = temporary;
    temporary = score;
}

void lastMoveBegin(unsigned int (&backupVals)[4][4], unsigned int (&tempVals)[4][4], unsigned int inputVals [4][4], unsigned int &backupScore, unsigned int &temporary, unsigned int score)
{
    for (int x = 0; x < 4; x++){
        for (int y = 0; y  < 4; y++){
            backupVals[x][y] = inputVals[x][y];
            tempVals[x][y] = inputVals[x][y];
        }
    }

    backupScore = score;
    temporary = score;
}

void restoreLastMove(unsigned int (&inputVals)[4][4], unsigned int (&tempVals)[4][4], unsigned int backupVals [4][4], unsigned int &score, unsigned int &temporary, unsigned int backupScore)
{
    for (int x = 0; x < 4; x++){
        for (int y = 0; y  < 4; y++){
            inputVals[x][y] = backupVals[x][y];
            tempVals[x][y] = backupVals[x][y];
        }
    }

    score = backupScore;
    temporary = backupScore;
}
